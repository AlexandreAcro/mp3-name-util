import os
for root, dirs, files in os.walk("."):  
    for filename in files:
        fname, ext = os.path.splitext(filename)
        if ext == ".mp3":
            spl = fname.split(" - ")
            if (len(spl) == 2):
                os.rename(os.getcwd() + '/' + filename, os.getcwd() + '/' + spl[1] + " (" + spl[0] + ")" + ext)
            elif (len(spl) == 1):
                spl = fname.split(" — ")
                if (len(spl) == 2):
                    os.rename(os.getcwd() + '/' + filename, os.getcwd() + '/' + spl[1] + " (" + spl[0] + ")" + ext)

os.remove(os.path.basename(__file__)) 
